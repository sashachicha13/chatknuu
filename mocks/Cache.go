// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// Cache is an autogenerated mock type for the Cache type
type Cache struct {
	mock.Mock
}

// Delete provides a mock function with given fields: ctx, key
func (_m *Cache) Delete(ctx context.Context, key string) {
	_m.Called(ctx, key)
}

// Get provides a mock function with given fields: ctx, key
func (_m *Cache) Get(ctx context.Context, key string) (interface{}, bool) {
	ret := _m.Called(ctx, key)

	var r0 interface{}
	if rf, ok := ret.Get(0).(func(context.Context, string) interface{}); ok {
		r0 = rf(ctx, key)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(interface{})
		}
	}

	var r1 bool
	if rf, ok := ret.Get(1).(func(context.Context, string) bool); ok {
		r1 = rf(ctx, key)
	} else {
		r1 = ret.Get(1).(bool)
	}

	return r0, r1
}

// Set provides a mock function with given fields: ctx, key, value
func (_m *Cache) Set(ctx context.Context, key string, value interface{}) {
	_m.Called(ctx, key, value)
}
