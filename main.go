package main

import (
	"context"
	"net/http"

	firebase "firebase.google.com/go/v4"
	"google.golang.org/api/option"

	"github.com/gin-gonic/gin"

	chat "github.com/ChatKNU/pkg/api"
	"github.com/ChatKNU/pkg/db"
	"github.com/ChatKNU/pkg/logger"
	"github.com/ChatKNU/pkg/mdlwr"
	"github.com/ChatKNU/pkg/utils"
)

func main() {
	logger.InitLog(false, "chatKNU-api")
	gin.SetMode(gin.ReleaseMode)

	ctx := context.Background()
	logs := logger.FromContext(ctx)

	logs.Info("service starting")

	opt := option.WithCredentialsFile("./empirical-radio-246713-firebase-adminsdk-niofb-fc768e3ab2.json")
	firebaseApp, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		logs.WithError(err).Panic("failed to create firebase application")
	}

	firebaseDatabase, err := firebaseApp.DatabaseWithURL(ctx, "https://empirical-radio-246713-default-rtdb.europe-west1.firebasedatabase.app")
	if err != nil {
		logs.WithError(err).Panic("failed to create firebase realtime database")
	}

	firebaseClient, err := firebaseApp.Auth(context.Background())
	if err != nil {
		logs.WithError(err).Panic("failed to create firebase auth client")
	}

	verifier := mdlwr.NewFirebaseAuthTokenVerifier(firebaseClient)
	authMdlwr := mdlwr.NewAuthMdlwr(verifier)

	DB, err := db.NewDB(context.Background(), firebaseDatabase, "empirical-radio-246713", false, opt)
	if err != nil {
		logs.WithError(err).Panic("failed to open repository")
	}
	defer DB.Close()

	router := gin.New()
	router.Use(gin.Recovery())

	chatConfig := chat.Config{
		Db:            DB,
		IdGen:         utils.UUIDIDGen{},
	}
	chat.AssembleService(router, authMdlwr, chatConfig)

	router.GET("/api/version", func(c *gin.Context) {
		versions := map[string]string{
			"api":    "0.0.1",
			"status": "green",
		}
		c.JSON(http.StatusOK, versions)
	})

	err = router.Run()
	if err != nil {
		logs.WithError(err).Panic("failed to start router")
	}
}