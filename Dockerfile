# Start from the latest golang base image        900 mb
FROM golang:latest as builder

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy the source from the current directory to the Working Directory inside the container
COPY . .

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -a -o chatknuu .

######## Start a new stage from scratch ########       30 mb Li
FROM alpine:latest

RUN apk add --no-cache tzdata
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apk --no-cache add ca-certificates

WORKDIR /root/
COPY /empirical-radio-246713-firebase-adminsdk-niofb-fc768e3ab2.json .

# Copy the Pre-built binary file from the previous stage
COPY --from=builder /app/chatknuu .

EXPOSE 8080

# Command to run the executable
CMD ["./chatknuu"]
