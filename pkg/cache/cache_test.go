package cache_test

import (
	"context"
	"testing"
	"time"

	"github.com/ChatKNU/pkg/cache"
	"github.com/stretchr/testify/assert"
)

func TestCache(t *testing.T) {
	ctx := context.Background()
	c := cache.New(10, 2)

	c.Set(ctx, "key-1", "value-1")
	c.Set(ctx, "key-2", "value-2")

	value, found := c.Get(ctx, "key-1")
	assert.True(t, found)
	assert.Equal(t, "value-1", value.(string))

	value, found = c.Get(ctx, "key-2")
	assert.True(t, found)
	assert.Equal(t, "value-2", value.(string))

	c.Delete(ctx, "key-1")

	_, found = c.Get(ctx, "key-1")
	assert.False(t, found)

	time.Sleep(time.Second * 2)

	_, found = c.Get(ctx, "key-2")
	assert.False(t, found)
}

