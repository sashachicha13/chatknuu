package cache

import (
	"context"
	"time"

	"github.com/ChatKNU/pkg/logger"
	"github.com/bluele/gcache"
)


type Cache interface {
	Get(ctx context.Context, key string) (interface{}, bool)
	Set(ctx context.Context, key string, value interface{})
	Delete(ctx context.Context, key string)
}

// size - cache capacity
// expiration - cache expiration time in seconds
func New(size int, expiration int) Cache {
	return &cache{
		gc:   gcache.New(size).LRU().Build(),
		expr: expiration,
	}
}

type cache struct {
	gc   gcache.Cache
	expr int
}

func (c *cache) Get(ctx context.Context, key string) (interface{}, bool) {
	val, err := c.gc.Get(key)
	if err != nil && err != gcache.KeyNotFoundError {
		logger.FromContext(ctx).WithError(err).Error("failed to read value from cache")
	}
	return val, err == nil
}

func (c *cache) Set(ctx context.Context, key string, value interface{}) {
	err := c.gc.SetWithExpire(key, value, time.Second*time.Duration(c.expr))
	if err != nil {
		logger.FromContext(ctx).WithError(err).Error("failed to store value in cache")
	}
}

func (c *cache) Delete(ctx context.Context, key string) {
	c.gc.Remove(key)
}

