package chat

import (
	"net/http"

	"github.com/ChatKNU/pkg/logger"
	"github.com/ChatKNU/pkg/types"
	"github.com/ChatKNU/pkg/utils"
	"github.com/gavrilaf/errors"
	"github.com/gin-gonic/gin"
)

type apiHandler struct {
	manager Manager
}

func newHandler(manager Manager) *apiHandler {
	return &apiHandler{manager: manager}
}

func (h *apiHandler) sendmessage(c *gin.Context) {
	ctx := utils.ContextFromGinContext(c)

	userID := c.Param("user_id")
	chatID := c.Param("id")

	var msg types.IncomingChatMessage

	err := c.Bind(&msg)
	if err != nil {
		err = errors.NewBadRequest(err, "invalid json")
		logger.FromContext(ctx).WithError(err).Error("failed to send message")
		utils.APIError(c, err)
		return
	}

	err = h.manager.SendMessage(ctx, userID, chatID, msg)
	if err != nil {
		logger.FromContext(ctx).WithError(err).Error("failed to send chat message")
		utils.APIError(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
	})
}

func (h *apiHandler) getmessage(c *gin.Context) {
	chatID := c.Param("id")
	ctx := utils.ContextFromGinContext(c)

	var query types.ChatQuery
	if err := c.ShouldBindQuery(&query); err != nil {
		err = errors.NewBadRequest(err, "invalid query string")
		logger.FromContext(ctx).WithError(err).Error("failed to read chat messages")
		utils.APIError(c, err)
		return
	}

	result, err := h.manager.GetMessages(ctx, chatID, query)
	if err != nil {
		logger.FromContext(ctx).WithError(err).Error("failed to read chat messages")
		utils.APIError(c, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (h *apiHandler) get(c *gin.Context) {
	ctx := utils.ContextFromGinContext(c)

	userID := c.Param("user_id")
	chatID := c.Param("id")

	chat, err := h.manager.GetChat(ctx, userID, chatID)
	if err != nil {
		logger.FromContext(ctx).WithError(err).Error("failed to read chat")
		utils.APIError(c, err)
		return
	}

	c.JSON(http.StatusOK, chat)
}

func (h *apiHandler) create(c *gin.Context) {
	userID := c.Param("user_id")
	ctx := utils.ContextFromGinContext(c)

	var dto types.ChatDTO
	err := c.Bind(&dto)
	if err != nil {
		err = errors.NewBadRequest(err, "invalid json")
		logger.FromContext(ctx).WithError(err).Error("failed to create chat")
		utils.APIError(c, err)
		return
	}

	chat, err := h.manager.CreateChat(ctx, userID, dto)
	if err != nil {
		logger.FromContext(ctx).WithError(err).Error("failed to create chat")
		utils.APIError(c, err)
		return
	}

	c.JSON(http.StatusOK, chat)
}

func (h *apiHandler) close(c *gin.Context) {
	userID := c.Param("user_id")
	ctx := utils.ContextFromGinContext(c)

	chatID := c.Param("id")

	err := h.manager.CloseChat(ctx, userID, chatID)
	if err != nil {
		logger.FromContext(ctx).WithError(err).Error("failed to close chat")
		utils.APIError(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
	})
}

func (h *apiHandler) join(c *gin.Context) {
	userID := c.Param("user_id")
	ctx := utils.ContextFromGinContext(c)

	chatID := c.Param("id")

	err := h.manager.JoinChat(ctx, userID, chatID)
	if err != nil {
		logger.FromContext(ctx).WithError(err).Error("failed to join chat")
		utils.APIError(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
	})
}

func (h *apiHandler) leave(c *gin.Context) {
	userID := c.Param("user_id")
	ctx := utils.ContextFromGinContext(c)

	chatID := c.Param("id")

	err := h.manager.LeaveChat(ctx, userID, chatID)
	if err != nil {
		logger.FromContext(ctx).WithError(err).Error("failed to leave chat")
		utils.APIError(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
	})
}
