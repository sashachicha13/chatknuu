package chat

import (
	"github.com/ChatKNU/pkg/mdlwr"
	"github.com/gin-gonic/gin"
)

func AssembleService(router *gin.Engine, authMdlwr *mdlwr.AuthMdlwr, config Config) Manager {
	chatManager := NewManager(config)
	InitRouter(router, chatManager, authMdlwr.MiddlewareFunc())

	return chatManager
}
