package chat

import (
	"context"
	"time"

	"github.com/ChatKNU/pkg/db"
	"github.com/ChatKNU/pkg/logger"
	"github.com/ChatKNU/pkg/types"
	"github.com/ChatKNU/pkg/utils"
	"github.com/gavrilaf/errors"
	"github.com/sirupsen/logrus"
)

//go:generate mockery -name Manager -outpkg chatmocks -output ./chatmocks -dir .
type Manager interface {
	SendMessage(ctx context.Context, userID string, chatID string, msg types.IncomingChatMessage) error
	GetMessages(ctx context.Context, chatID string, query types.ChatQuery) (*types.ChatPageOut, error)

	CreateChat(ctx context.Context, userID string, dto types.ChatDTO) (types.ChatDTO, error)
	CloseChat(ctx context.Context, userID string, chatID string) error

	JoinChat(ctx context.Context, userID string, chatID string) error
	LeaveChat(ctx context.Context, userID string, chatID string) error

	GetChat(ctx context.Context, userID string, chatID string) (types.ChatDTO, error)
}

type Config struct {
	Db    db.DB
	IdGen utils.IDGen
}

func NewManager(config Config) Manager {
	return &managerImpl{
		db:    config.Db,
		idGen: config.IdGen,
	}
}

type managerImpl struct {
	db    db.DB
	idGen utils.IDGen
}

func (man *managerImpl) SendMessage(ctx context.Context, userID string, chatID string, msg types.IncomingChatMessage) error {
	t := logger.NewTraceWithFields(ctx, "SendMessage", logrus.Fields{"user-id": userID, "chat-id": chatID})
	defer t.Done()

	err := man.db.ChatAddMessage(ctx, types.IncomingChatMessage{
		ChatID:   chatID,
		SenderID: userID,
		IsBot:    false,
		Text:     msg.Text,
	})

	return err
}

func (man *managerImpl) GetMessages(ctx context.Context, chatID string, query types.ChatQuery) (*types.ChatPageOut, error) {
	t := logger.NewTraceWithFields(ctx, "GetMessages", logrus.Fields{"chat-id": chatID,
		"start-from": query.StartFrom,
		"limit":      query.Limit})
	defer t.Done()

	var startFrom *time.Time
	if query.StartFrom != 0 {
		t := time.Unix(0, query.StartFrom)
		startFrom = &t
	}

	page, err := man.db.ChatGetMessages(ctx, chatID, startFrom, query.Limit)
	if err != nil {
		return nil, err
	}

	logger.FromContext(ctx).Infof("read chat page, query=%v, messages=%d, attendees=%d", query, len(page.Messages), len(page.Attendees))

	messages := make([]types.OutgoingChatMessage, 0, len(page.Messages))
	for _, m := range page.Messages {
		messages = append(messages, types.OutgoingChatMessage{
			ID:        m.ID,
			SenderID:  m.SenderID,
			Text:      m.Text,
			Timestamp: m.Timestamp.Format(time.RFC3339),
		})
	}

	var lastTimestamp int64 = 0
	if page.LastTimestamp != nil {
		lastTimestamp = page.LastTimestamp.UnixNano()
	}

	result := &types.ChatPageOut{
		Start:     query.StartFrom,
		End:       lastTimestamp,
		Messages:  messages,
		Attendees: page.Attendees,
	}

	return result, nil
}

func (man *managerImpl) GetChat(ctx context.Context, userID string, chatID string) (types.ChatDTO, error) {
	t := logger.NewTraceWithFields(ctx, "GetChat", logrus.Fields{"user-id": userID, "chat-id": chatID})
	defer t.Done()

	mdl, err := man.db.GetChatFullInfo(ctx, chatID)
	if err != nil {
		t.Log.WithError(err).Error("failed to read chat")
		return types.ChatDTO{}, err
	}

	dto := types.NewChatDTOFull(mdl)
	return dto, nil
}

func (man *managerImpl) GetUserChats(ctx context.Context, userID string) (types.UserChatsDTO, error) {
	t := logger.NewTraceWithField(ctx, "GetUserChats", "user-id", userID)
	defer t.Done()

	chats, err := man.db.GetUserChats(ctx, userID)
	if err != nil {
		t.Log.WithError(err).Error("failed to read user chats")
		return types.UserChatsDTO{}, err
	}

	t.Log.Infof("user chats: created=%d, joined=%d", len(chats.Created), len(chats.Joined))

	created := make([]types.ChatDTO, len(chats.Created))
	for indx, mdl := range chats.Created {
		created[indx] = types.NewChatDTOFull(mdl)
	}

	joined := make([]types.ChatDTO, len(chats.Joined))
	for indx, mdl := range chats.Joined {
		joined[indx] = types.NewChatDTOFull(mdl)
	}

	// TODO: answer by socket here

	return types.UserChatsDTO{
		Created: created,
		Joined:  joined,
	}, nil
}

func (man *managerImpl) CreateChat(ctx context.Context, userID string, dto types.ChatDTO) (types.ChatDTO, error) {
	t := logger.NewTraceWithField(ctx, "CreateChat", "user-id", userID)
	defer t.Done()

	_, err := man.db.GetProfile(ctx, userID)
	if err != nil {
		t.Log.WithError(err).Error("failed to read user profile")
		return types.ChatDTO{}, err
	}

	mdl, err := types.NewChatRepo(dto)
	if err != nil {
		t.Log.WithError(err).Error("failed to convert to model")
		return types.ChatDTO{}, err
	}

	mdl.CreatorID = userID
	mdl.Closed = false

	mdl, err = man.db.CreateChat(ctx, mdl)
	if err != nil {
		t.Log.WithError(err).Error("failed to create chat")
		return types.ChatDTO{}, err
	}

	t.Log.WithField("chat-id", mdl.ID).Info("chat created")

	newDto := types.NewChatDTO(mdl)

	newDto.Owner = types.Attendee{UserID: userID, Name: ""}

	return newDto, nil
}

func (man *managerImpl) CloseChat(ctx context.Context, userID string, chatID string) error {
	t := logger.NewTraceWithFields(ctx, "CloseChat", logrus.Fields{"user-id": userID, "chat-id": chatID})
	defer t.Done()

	fullInfo, err := man.db.GetChatFullInfo(ctx, chatID)
	if err != nil {
		return err
	}

	if fullInfo.Chat.CreatorID != userID {
		err = errors.Forbiddenf("close is allowed only for owner")
		t.Log.WithError(err).Error("call is forbidden for the user")
		return err
	}

	if len(fullInfo.Attendees) == 0 {
		if err = man.db.DeleteChat(ctx, chatID); err != nil {
			t.Log.WithError(err).Error("failed to delete chat")
			return err
		}
		return nil
	}

	mdl := fullInfo.Chat
	mdl.Closed = true
	if err = man.db.UpdateChat(ctx, mdl); err != nil {
		t.Log.WithError(err).Error("failed to update chat")
		return err
	}

	return nil
}

// Join/Left
func (man *managerImpl) JoinChat(ctx context.Context, userID string, chatID string) error {
	t := logger.NewTraceWithFields(ctx, "JoinChat", logrus.Fields{"user-id": userID, "chat-id": chatID})
	defer t.Done()

	_, err := man.db.GetProfile(ctx, userID)
	if err != nil {
		t.Log.WithError(err).Error("failed to read user profile")
		return err
	}

	mdl, err := man.checkedGetChat(ctx, t.Log, chatID)
	if err != nil {
		return err
	}

	if mdl.CreatorID == userID {
		err = errors.MethodNotAllowedf("user already owner")
		t.Log.WithError(err).Error("not allowed, user is already owner")
	}

	// TODO: Check if user is already in attendees

	addTrace := logger.NewTrace(ctx, "JoinChat.AddToChatAttendees")
	err = man.db.AddToChatAttendees(ctx, userID, chatID)
	if err != nil {
		t.Log.WithError(err).Error("failed to add to attendees")
		return err
	}
	addTrace.Done()

	return nil
}

func (man *managerImpl) LeaveChat(ctx context.Context, userID string, chatID string) error {
	t := logger.NewTraceWithFields(ctx, "LeaveChat", logrus.Fields{"user-id": userID, "chat-id": chatID})
	defer t.Done()

	_, err := man.db.GetChat(ctx, chatID)
	if err != nil {
		t.Log.WithError(err).Error("failed to get chat")
		return err
	}

	err = man.db.RemoveFromChatAttendees(ctx, userID, chatID)
	if err != nil {
		t.Log.WithError(err).Error("failed to remove from attendees")
		return err
	}

	return nil
}

func (man *managerImpl) checkedGetChat(ctx context.Context, l *logrus.Entry, id string) (types.Chat, error) {
	mdl, err := man.db.GetChat(ctx, id)
	if err != nil {
		l.WithError(err).Error("failed to read chat")
		return types.Chat{}, err
	}

	if mdl.Closed {
		err = errors.MethodNotAllowedf("closed")
		l.WithError(err).Error("chat is closed, call is not allowed")
		return types.Chat{}, err
	}

	return mdl, nil
}
