package chat

import (
	"context"
	"time"

	"github.com/ChatKNU/pkg/types"
)

//go:generate mockery -name DB -outpkg chatmocks -output ./chatmocks -dir .
type DB interface {
	ChatAddMessage(ctx context.Context, msg types.IncomingChatMessage) error
	ChatGetMessages(ctx context.Context, chatID string, startFrom *time.Time, limit int) (types.ChatPage, error)
}