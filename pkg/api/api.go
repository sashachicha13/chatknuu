package chat

import (
	"github.com/gin-gonic/gin"
)

func InitRouter(router *gin.Engine, manager Manager, authMdlwr gin.HandlerFunc) {
	handler := newHandler(manager)

	gr := router.Group("/api/v1/chat", authMdlwr)
	{
		gr.GET("/chat/:id", handler.get) // get chat by id

		gr.POST("/chat/:user_id", handler.create)      // create chat
		gr.DELETE("/chat/:id/:user_id", handler.close) // close chat

		gr.PUT("/chat/:id/attendees/:user_id", handler.join)     // join to the chat
		gr.DELETE("/chat/:id/attendees/:user_id", handler.leave) // leave the chat

		gr.GET("/chat/message/:id", handler.getmessage)            // get message
		gr.POST("/chat/message/:id/:user_id", handler.sendmessage) // send message
	}
}
