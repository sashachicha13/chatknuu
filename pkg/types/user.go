package types

import (
	"errors"
	"fmt"
)

var ErrInvalidAge = errors.New("age must be over 13")

type UserProfile struct {
	UserID    string `firestore:"user_id"`
	Name      string `firestore:"name"`
	Email     string `firestore:"email"`
	PushToken string `firestore:"push_token"`
}

func (p UserProfile) String() string {
	return fmt.Sprintf("UserProfile(%s, %s)", p.UserID, p.Name)
}
