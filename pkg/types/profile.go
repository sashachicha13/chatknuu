package types

import (
	"fmt"
)

type UserProfileDTO struct {
	UserID string `json:"user_id"`
	Name   string `json:"name" binding:"required,max=1024"`
	Email  string `json:"email" binding:"max=512"`
}

func (p *UserProfileDTO) String() string {
	return fmt.Sprintf("UserProfileDTO(%s, %s)", p.UserID, p.Name)
}

func NewProfileDTO(p UserProfile) UserProfileDTO {
	return UserProfileDTO{
		UserID: p.UserID,
		Name:   p.Name,
		Email:  p.Email,
	}
}

func NewProfileRepo(dto UserProfileDTO) (UserProfile, error) {

	return UserProfile{
		UserID: dto.UserID,
		Name:   dto.Name,
		Email:  dto.Email,
	}, nil
}
