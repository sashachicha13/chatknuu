package types

type OutgoingChatMessage struct {
	ID          string `json:"id"`
	SenderID    string `json:"sender_id"`
	Text        string `json:"text"`
	Timestamp   string `json:"timestamp"`
}

type ChatPageOut struct {
	Start     int64                 `json:"start"`
	End       int64                 `json:"end"`
	Messages  []OutgoingChatMessage `json:"messages"`
	Attendees []Attendee            `json:"attendees"`
}
