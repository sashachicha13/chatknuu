package types

import "time"

type IncomingChatMessage struct {
	ChatID   string `json:"chat_id"`
	SenderID string `json:"sender_id"`
	IsBot    bool   `json:"is_bot"`
	LocalID  string `json:"local_id"`
	Text     string `json:"text"`
}

type ChatMessage struct {
	ID        string    `firestore:"id"`
	Timestamp time.Time `firestore:"timestamp"`
	SenderID  string    `firestore:"sender_id"`
	IsBot     bool      `firestore:"is_bot"`
	Text      string    `firestore:"text"`
}

type ChatPage struct {
	Messages      []ChatMessage
	Attendees     []Attendee
	LastTimestamp *time.Time
}

type Attendee struct {
	UserID string `json:"user_id"`
	Name   string `json:"name"`
}

type ChatQuery struct {
	StartFrom int64 `form:"start_from"`
	Limit     int   `form:"limit"`
}
