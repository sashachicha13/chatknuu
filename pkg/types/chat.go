package types

import (
	"fmt"
)

type Chat struct {
	ID        string `firestore:"id"`
	CreatorID string `firestore:"creator_id"`
	Name      string `firestore:"name"`
	Closed    bool   `firestore:"closed"`
}

type UserChats struct {
	Created []ChatFullInfo
	Joined  []ChatFullInfo
}

type ChatFullInfo struct {
	Chat      Chat
	Owner     Attendee
	Attendees []Attendee
}

type ChatDTO struct {
	ID        string     `json:"id"`
	CreatorID string     `json:"creator_id"`
	Attendees []Attendee `json:"attendees"`
	Name      string     `json:"name" binding:"required,max=1024"`
	Closed    bool       `json:"closed"`
	Owner     Attendee   `json:"owner"`
}

type UserChatsDTO struct {
	Created []ChatDTO `json:"created"`
	Joined  []ChatDTO `json:"joined"`
}

func (p *ChatDTO) String() string {
	return fmt.Sprintf("ChatDTO(%s, %s, %s)", p.ID, p.CreatorID, p.Name)
}

func NewChatRepo(dto ChatDTO) (Chat, error) {
	return Chat{
		ID:        dto.ID,
		CreatorID: dto.CreatorID,
		Name:      dto.Name,
		Closed:    dto.Closed,
	}, nil
}

func NewChatDTOFull(p ChatFullInfo) ChatDTO {
	return ChatDTO{
		ID:        p.Chat.ID,
		CreatorID: p.Chat.CreatorID,
		Name:      p.Chat.Name,
		Closed:    p.Chat.Closed,
		Owner:     p.Owner,
		Attendees: p.Attendees,
	}
}

func NewChatDTO(p Chat) ChatDTO {
	return ChatDTO{
		ID:        p.ID,
		CreatorID: p.CreatorID,
		Name:      p.Name,
		Closed:    p.Closed,
	}
}

// Attendee
func NewAttendeeFromProfile(profile UserProfile) Attendee {
	return Attendee{
		UserID: profile.UserID,
		Name:   profile.Name,
	}
}
