package types

import "encoding/json"

type DirectMessage struct {
	Users []string          `json:"users"`
	Title string            `json:"title"`
	Text  string            `json:"text"`
	Data  map[string]string `json:"data"`
}

func UnmarshalDirectMessage(data []byte) (DirectMessage, error) {
	var msg DirectMessage
	err := json.Unmarshal(data, &msg)

	return msg, err
}

func (msg DirectMessage) Marshal() ([]byte, error) {
	return json.Marshal(msg)
}
