package types

import (
	"fmt"
	"time"
)

type NotificationType int

const (
	NotificationCreateChat NotificationType = iota + 1
	NotificationEventChatMsg
	NotificationUserJoinChat
	NotificationUserLeaveChat
	NotificationUserCloseChat
)

var ErrUnsupportedNotificationType = fmt.Errorf("unsupported event")

type Notification struct {
	ID        string
	Type      NotificationType `firestore:"type"`
	EventID   string           `firestore:"event_id"`
	UserID    string           `firestore:"user_id"`
	Timestamp time.Time        `firestore:"timestamp"`
}

// ModelDidUpdateMsg

const (
	UserJoinChatMsgType = iota + 1
	UserLeaveChatMsgType
	UserCloseChatMsgType

	CreateChatMsgType

	EventChatUpdateMsgType
)

type ModelUpdateMsg struct {
	MsgType int    `json:"msg_type"`
	UserID  string `json:"user_id"`
	ChatID string `json:"event_id,omitempty"`
}
