package mdlwr

import (
	"context"

	"firebase.google.com/go/v4/auth"
)

type firebaseTokenVerifier struct {
	client *auth.Client
}

func NewFirebaseAuthTokenVerifier(client *auth.Client) AuthMdlwrTokenVerifier {
	return firebaseTokenVerifier{client: client}
}

func (verifier firebaseTokenVerifier) VerifyToken(idToken string) (string, error) {
	token, err := verifier.client.VerifyIDToken(context.Background(), idToken)
	if err == nil {
		return token.UID, nil
	} else {
		return "", err
	}
}
