package mdlwr

import (
	"strings"

	"github.com/ChatKNU/pkg/utils"
	"github.com/gavrilaf/errors"
	"github.com/gin-gonic/gin"
)

const (
	authToken     = "Authorization"
	authTokenHead = "Bearer"
)

type AuthMdlwrTokenVerifier interface {
	VerifyToken(idToken string) (string, error)
}

type AuthMdlwr struct {
	verifier AuthMdlwrTokenVerifier
}

func NewAuthMdlwr(verifier AuthMdlwrTokenVerifier) *AuthMdlwr {
	return &AuthMdlwr{verifier: verifier}
}

// Authentication middleware
// Extract bearer token from the headers and check it

func (mdlwr *AuthMdlwr) MiddlewareFunc() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.Request.Header.Get(authToken)
		if len(authHeader) == 0 {
			utils.APIError(c, errors.BadRequestf("Bearer token not found"))
			return
		}

		parts := strings.SplitN(authHeader, " ", 2)
		if !(len(parts) == 2 && parts[0] == authTokenHead) {
			utils.APIError(c, errors.BadRequestf("Invalid Bearer token"))
			return
		}

		tokenStr := parts[1]
		_, err := mdlwr.verifier.VerifyToken(tokenStr)

		if err != nil {
			utils.APIError(c, errors.NewUnauthorized(err, "token validating error"))
			return
		}

		c.Next()
	}
}
