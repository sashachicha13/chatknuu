package mdlwr

import (
	"fmt"
)

const (
	FakeValidAuthToken       = "valid-token"
	FakeValiBearerdAuthToken = "Bearer valid-token"

	FakeValidUserID = "fake-valid-user"
)

func NewFakeAuthTokenVerifier() AuthMdlwrTokenVerifier {
	return fakeAuthTokenVerifier{}
}

/////////////////////////////////////////////////////////////////////////////////////

type fakeAuthTokenVerifier struct{}

func (fakeAuthTokenVerifier) VerifyToken(idToken string) (string, error) {
	if idToken == FakeValidAuthToken {
		return FakeValidUserID, nil
	} else {
		return "", fmt.Errorf("invalid token")
	}
}
