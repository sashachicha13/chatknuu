package mdlwr

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestMdlwrAuth(t *testing.T) {
	tests := []struct {
		name   string
		header string
		code   int
	}{
		{"no token", "", http.StatusBadRequest},
		{"invalid token", "Bearrrrer", http.StatusBadRequest},
		{"expired token", "Bearer invalid-token", http.StatusUnauthorized},
		{"valid token", FakeValiBearerdAuthToken, http.StatusOK},
	}

	mdlwr := NewAuthMdlwr(NewFakeAuthTokenVerifier())

	gin.SetMode(gin.ReleaseMode)
	gin.DefaultWriter = ioutil.Discard
	router := gin.New()

	router.GET("/token", mdlwr.MiddlewareFunc(), func(c *gin.Context) {
		c.Status(200)
	})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := httptest.NewRecorder()

			req, _ := http.NewRequest("GET", "/token", nil)
			if len(tt.header) > 0 {
				req.Header.Set("Authorization", tt.header)
			}
			router.ServeHTTP(w, req)

			assert.Equal(t, tt.code, w.Code)
		})
	}
}

