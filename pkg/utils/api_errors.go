package utils

import (
	"net/http"

	"github.com/gavrilaf/errors"
	"github.com/gin-gonic/gin"
)

func APIError(c *gin.Context, err error) {
	switch {
	case errors.IsNotFound(err):
		APIErrorWithStatus(c, http.StatusNotFound, err)
	case errors.IsBadRequest(err):
		APIErrorWithStatus(c, http.StatusBadRequest, err)
	case errors.IsUnauthorized(err):
		APIErrorWithStatus(c, http.StatusUnauthorized, err)
	default:
		APIErrorWithStatus(c, http.StatusInternalServerError, err)
	}
}

func APIErrorWithStatus(c *gin.Context, status int, err error) {
	c.AbortWithStatusJSON(status, gin.H{
		"error": err.Error(),
	})
}
