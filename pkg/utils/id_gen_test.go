package utils_test

import (
	"testing"

	"github.com/ChatKNU/pkg/utils"
	"github.com/stretchr/testify/assert"
)

func TestIDGen(t *testing.T) {
	var idGen utils.IDGen = utils.UUIDIDGen{}

	id1 := idGen.StringID()
	id2 := idGen.StringID()

	assert.NotEmpty(t, id1)
	assert.NotEmpty(t, id2)
	assert.NotEqual(t, id1, id2)
}



