package utils

import (
	"context"

	"github.com/gin-gonic/gin"
)

// context

func ContextFromGinContext(c *gin.Context) context.Context {
	return c.Request.Context()
}

