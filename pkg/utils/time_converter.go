package utils

import (
	"fmt"
	"time"

	"cloud.google.com/go/civil"

	"github.com/gavrilaf/errors"
)

const (
	emptyDate = "0000-00-00"
)

func IsEmptyDateStr(s string) bool {
	if len(s) == 0 || s == emptyDate {
		return true
	}
	return false
}

func FormatDate(dt civil.Date) string {
	if dt.Year == 0 {
		return ""
	}
	return dt.String()
}

func FormatTime(tm civil.Time) string {
	return fmt.Sprintf("%02d:%02d", tm.Hour, tm.Minute)
}

func ParseDate(s string, allowEmpty bool) (civil.Date, error) {
	if IsEmptyDateStr(s) {
		if allowEmpty {
			return civil.Date{}, nil
		} else {
			return civil.Date{}, errors.NotValidf("empty date")
		}
	}
	return civil.ParseDate(s)
}

func ParseTime(s string) (civil.Time, error) {
	t, err := time.Parse("15:04", s)
	if err != nil {
		return civil.Time{}, err
	}
	return civil.TimeOf(t), nil
}
