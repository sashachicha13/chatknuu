package logger

import (
	"context"

	stackdriver "github.com/TV4/logrus-stackdriver-formatter"
	"github.com/sirupsen/logrus"
)

func newLoggerEntry(local bool, service string) *logrus.Entry {
	logger := logrus.StandardLogger()
	if local {
		logger.SetFormatter(&logrus.TextFormatter{
			ForceColors: true,
		})

		entry := logrus.NewEntry(logger)
		if service != "" {
			entry = entry.WithField("service", service)
		}

		return entry
	}

	if service != "" {
		logger.SetFormatter(stackdriver.NewFormatter(stackdriver.WithService(service)))
	} else {
		logger.SetFormatter(stackdriver.NewFormatter())
	}

	return logrus.NewEntry(logger)
}

var (
	L = newLoggerEntry(true, "")
)

type logKey struct{}

func InitLog(local bool, service string) {
	L = newLoggerEntry(local, service)
}

func FromContext(ctx context.Context) *logrus.Entry {
	entry := ctx.Value(logKey{})
	if entry == nil {
		return L
	}

	return entry.(*logrus.Entry)
}
