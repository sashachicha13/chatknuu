package db

import (
	"context"
	"fmt"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/iterator"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/ChatKNU/pkg/logger"
	"github.com/ChatKNU/pkg/types"
	"github.com/gavrilaf/errors"
)

func (db *dbImpl) GetProfile(ctx context.Context, userID string) (types.UserProfile, error) {
	t := logger.NewTrace(ctx, "db.GetProfile")
	defer t.Done()

	dsnap, err := db.users.Doc(userID).Get(ctx)
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return types.UserProfile{}, errors.NotFoundf("profile not found, userID=%s", userID)
		} else {
			return types.UserProfile{}, err
		}
	}

	var mdl types.UserProfile
	err = dsnap.DataTo(&mdl)
	mdl.UserID = userID

	return mdl, err
}

func (db *dbImpl) UpdateProfile(ctx context.Context, profile types.UserProfile) error {
	profileMap := map[string]interface{}{
		"user_id": profile.UserID,
		"name":    profile.Name,
		"email":   profile.Email,
	}

	_, err := db.users.Doc(profile.UserID).Set(ctx, profileMap, firestore.MergeAll)
	return err
}

func (db *dbImpl) DeleteProfile(ctx context.Context, userID string) error {
	err := deleteCollection(ctx, db.fs, db.users.Doc(userID).Collection("profiles"), 100)
	if err != nil {
		return err
	}

	_, err = db.users.Doc(userID).Delete(ctx)

	return err
}

func deleteCollection(ctx context.Context, client *firestore.Client, ref *firestore.CollectionRef, batchSize int) error {
	t := logger.NewTrace(ctx, fmt.Sprintf("db.deleteCollection: %s", ref.ID))
	defer t.Done()

	for {
		// Get a batch of documents
		iter := ref.Limit(batchSize).Documents(ctx)
		numDeleted := 0

		// Iterate through the documents, adding
		// a delete operation for each one to a
		// WriteBatch.
		batch := client.Batch()
		for {
			doc, err := iter.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return err
			}

			batch.Delete(doc.Ref)
			numDeleted++
		}

		// If there are no documents to delete,
		// the process is over.
		if numDeleted == 0 {
			return nil
		}

		logger.FromContext(ctx).Infof("delete-collection, deleting %d recors", numDeleted)
		_, err := batch.Commit(ctx)
		if err != nil {
			return err
		}
	}
}
