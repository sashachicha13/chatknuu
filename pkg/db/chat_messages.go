package db

import (
	"context"
	"log"
	"time"

	"cloud.google.com/go/firestore"

	"github.com/ChatKNU/pkg/logger"
	"github.com/ChatKNU/pkg/types"
)

const (
	subChat = "chat"
)

func (db *dbImpl) ChatAddMessage(ctx context.Context, msg types.IncomingChatMessage) error {
	msgMap := map[string]interface{}{
		"id":        msg.LocalID,
		"timestamp": firestore.ServerTimestamp,
		"sender_id": msg.SenderID,
		"is_bot":    msg.IsBot,
		"text":      msg.Text,
	}

	chat := db.chats.Doc(msg.ChatID).Collection(subChat)
	doc := chat.NewDoc()

	msgMap["id"] = doc.ID

	_, err := doc.Set(ctx, msgMap)
	if err != nil {
		return err
	}

	chatRef := db.realtimeRef.Child(msg.ChatID)
	err = chatRef.Set(ctx, msg)
	if err != nil {
		log.Fatalf("Error setting value %v to realtime database: %e", chatRef, err)
		return err
	}
	return err
}

func (db *dbImpl) ChatGetMessages(ctx context.Context, chatID string, startFrom *time.Time, limit int) (types.ChatPage, error) {
	chat := db.chats.Doc(chatID).Collection(subChat)

	page := chat.OrderBy("timestamp", firestore.Asc).Limit(limit)
	if startFrom != nil {
		page = page.StartAfter(*startFrom)
	}

	docs, err := page.Documents(ctx).GetAll()
	if err != nil {
		return types.ChatPage{}, err
	}

	messages := make([]types.ChatMessage, len(docs))
	attendeesIds := make(map[string]struct{})

	for indx, doc := range docs {
		var msg types.ChatMessage
		err := doc.DataTo(&msg)
		if err != nil {
			return types.ChatPage{}, err
		}
		messages[indx] = msg
		if !msg.IsBot && msg.SenderID != "" {
			attendeesIds[msg.SenderID] = struct{}{}
		}
	}

	attendees := make([]types.Attendee, 0, len(attendeesIds))

	for id := range attendeesIds {
		attendee, err := db.getAttendeeWithCache(ctx, id)
		if err != nil {
			return types.ChatPage{}, err
		}
		attendees = append(attendees, attendee)
	}

	var lastTimestamp *time.Time

	if len(messages) > 0 {
		lastTimestamp = &messages[len(messages)-1].Timestamp
	}

	result := types.ChatPage{
		Messages:      messages,
		Attendees:     attendees,
		LastTimestamp: lastTimestamp,
	}

	return result, nil
}

func (db *dbImpl) getAttendeeWithCache(ctx context.Context, userID string) (types.Attendee, error) {
	cacheKey := "a-" + userID
	cached, found := db.cache.Get(ctx, cacheKey)
	if found {
		logger.FromContext(ctx).Info("read attendee from cache")
		return cached.(types.Attendee), nil
	}

	user, err := db.GetProfile(ctx, userID)
	if err != nil {
		return types.Attendee{}, err
	}

	attendee := types.NewAttendeeFromProfile(user)
	db.cache.Set(ctx, cacheKey, attendee)

	return attendee, nil
}

func (db *dbImpl) getChatsByIds(ctx context.Context, ids []string) ([]types.ChatFullInfo, error) {
	if len(ids) == 0 {
		return []types.ChatFullInfo{}, nil
	}

	chatsRef, err := db.chats.Where("id", "in", ids).Documents(ctx).GetAll()
	if err != nil {
		return nil, err
	}

	chats := make([]types.ChatFullInfo, len(chatsRef))
	for indx, doc := range chatsRef {
		var chat types.Chat
		err = doc.DataTo(&chat)
		if err != nil {
			return nil, err
		}

		owner, err := db.getAttendeeWithCache(ctx, chat.CreatorID)
		if err != nil {
			return nil, err
		}

		attendees, err := db.getAttendeesFromSnapshot(ctx, doc)
		if err != nil {
			return nil, err
		}

		chats[indx] = types.ChatFullInfo{
			Chat:      chat,
			Owner:     owner,
			Attendees: attendees,
		}
	}

	return chats, nil
}

func (db *dbImpl) getAttendeesFromSnapshot(ctx context.Context, doc *firestore.DocumentSnapshot) ([]types.Attendee, error) {
	ids := idsArrayFromDoc(doc, "attendees")

	attendees := make([]types.Attendee, len(ids))
	for indx, id := range ids {
		attendee, err := db.getAttendeeWithCache(ctx, id)
		if err != nil {
			return nil, err
		}
		attendees[indx] = attendee
	}

	return attendees, nil
}

func idsArrayFromDoc(doc *firestore.DocumentSnapshot, field string) []string {
	m := doc.Data()
	a, ok := m[field].([]interface{})
	if !ok {
		return []string{}
	}

	var r []string
	if len(a) > 0 {
		r = make([]string, len(a))
		for indx, s := range a {
			r[indx] = s.(string)
		}
	}

	return r
}
