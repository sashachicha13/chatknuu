package db

import (
	"context"

	"cloud.google.com/go/firestore"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/ChatKNU/pkg/logger"
	"github.com/ChatKNU/pkg/types"
	"github.com/gavrilaf/errors"
)

func (db *dbImpl) CreateChat(ctx context.Context, chat types.Chat) (types.Chat, error) {
	ref := db.chats.Doc(chat.ID)
	if _, err := ref.Set(ctx, chat); err != nil {
		return types.Chat{}, err
	}
	return chat, nil
}

func (db *dbImpl) DeleteChat(ctx context.Context, id string) error {
	err := deleteCollection(ctx, db.fs, db.chats.Doc(id).Collection(subChat), 100)
	if err != nil {
		return err
	}

	_, err = db.chats.Doc(id).Delete(ctx)
	return err
}

func (db *dbImpl) GetChat(ctx context.Context, id string) (types.Chat, error) {
	dsnap, err := db.chats.Doc(id).Get(ctx)
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return types.Chat{}, errors.NotFoundf("chat not found, id = %s", id)
		} else {
			return types.Chat{}, err
		}
	}

	var mdl types.Chat
	err = dsnap.DataTo(&mdl)
	if err != nil {
		return types.Chat{}, err
	}

	return mdl, nil
}

func (db *dbImpl) UpdateChat(ctx context.Context, chat types.Chat) error {
	fieldsMap := map[string]interface{}{
		"name":   chat.Name,
		"closed": chat.Closed,
	}

	_, err := db.chats.Doc(chat.ID).Set(context.Background(), fieldsMap, firestore.MergeAll)
	return err
}

func (db *dbImpl) GetChatAttendees(ctx context.Context, chatID string) ([]types.Attendee, error) {
	doc, err := db.chats.Doc(chatID).Get(ctx)
	if err != nil {
		return nil, err
	}

	return db.getAttendeesFromSnapshot(ctx, doc)
}

func (db *dbImpl) GetChatFullInfo(ctx context.Context, chatID string) (types.ChatFullInfo, error) {
	t := logger.NewTrace(ctx, "db.GetChatFullInfo")
	defer t.Done()

	doc, err := db.chats.Doc(chatID).Get(ctx)
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return types.ChatFullInfo{}, errors.NotFoundf("chat not found, id = %s", chatID)
		} else {
			return types.ChatFullInfo{}, err
		}
	}

	var chat types.Chat
	err = doc.DataTo(&chat)
	if err != nil {
		return types.ChatFullInfo{}, err
	}

	owner, err := db.getAttendeeWithCache(ctx, chat.CreatorID)
	if err != nil {
		return types.ChatFullInfo{}, err
	}

	attendees, err := db.getAttendeesFromSnapshot(ctx, doc)
	if err != nil {
		return types.ChatFullInfo{}, err
	}

	return types.ChatFullInfo{
		Chat:      chat,
		Owner:     owner,
		Attendees: attendees,
	}, nil
}

func (db *dbImpl) GetUserChats(ctx context.Context, userID string) (types.UserChats, error) {
	t := logger.NewTrace(ctx, "db.GetUserChats")
	defer t.Done()

	// created chats
	createdChatsRef, err := db.chats.Where("creator_id", "==", userID).Documents(ctx).GetAll()
	if err != nil {
		return types.UserChats{}, err
	}

	owner, err := db.getAttendeeWithCache(ctx, userID)
	if err != nil {
		return types.UserChats{}, err
	}

	createdChats := make([]types.ChatFullInfo, len(createdChatsRef))
	for indx, createdDoc := range createdChatsRef {
		var chat types.Chat
		err = createdDoc.DataTo(&chat)
		if err != nil {
			return types.UserChats{}, err
		}

		attendees, err := db.getAttendeesFromSnapshot(ctx, createdDoc)
		if err != nil {
			return types.UserChats{}, err
		}

		createdChats[indx] = types.ChatFullInfo{
			Chat:      chat,
			Owner:     owner,
			Attendees: attendees,
		}
	}

	// joined chats
	userDoc, err := db.users.Doc(userID).Get(ctx)
	if err != nil {
		return types.UserChats{}, err
	}

	joinedIds := idsArrayFromDoc(userDoc, "joined_chats")
	joinedChats, err := db.getChatsByIds(ctx, joinedIds)
	if err != nil {
		return types.UserChats{}, err
	}

	return types.UserChats{
		Created: createdChats,
		Joined:  joinedChats,
	}, nil
}

func (db *dbImpl) AddToChatAttendees(ctx context.Context, userID string, chatID string) error {
	return db.fs.RunTransaction(ctx, func(ctx context.Context, tx *firestore.Transaction) error {
		err := tx.Update(db.chats.Doc(chatID), []firestore.Update{
			{Path: "attendees", Value: firestore.ArrayUnion(userID)},
		})
		if err != nil {
			return err
		}

		return tx.Update(db.users.Doc(userID), []firestore.Update{
			{Path: "joined_chats", Value: firestore.ArrayUnion(chatID)},
		})
	})
}

func (db *dbImpl) RemoveFromChatAttendees(ctx context.Context, userID string, chatID string) error {
	return db.fs.RunTransaction(ctx, func(ctx context.Context, tx *firestore.Transaction) error {
		err := tx.Update(db.chats.Doc(chatID), []firestore.Update{
			{Path: "attendees", Value: firestore.ArrayRemove(userID)},
		})
		if err != nil {
			return err
		}

		return tx.Update(db.users.Doc(userID), []firestore.Update{
			{Path: "joined_chats", Value: firestore.ArrayRemove(chatID)},
		})
	})
}
