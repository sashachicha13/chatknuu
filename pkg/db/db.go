package db

import (
	"context"
	"io"
	"time"

	"firebase.google.com/go/v4/db"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/option"

	"github.com/ChatKNU/pkg/cache"
	"github.com/ChatKNU/pkg/types"
	"github.com/ChatKNU/pkg/utils"
)

const (
	usersProdCollection = "Users"
	usersTestCollection = "TestUsers"

	chatsProdCollection = "Chats"
	chatsTestCollection = "TestChats"
)

type DB interface {
	io.Closer

	// profile
	GetProfile(ctx context.Context, userID string) (types.UserProfile, error)
	UpdateProfile(ctx context.Context, profile types.UserProfile) error
	DeleteProfile(ctx context.Context, userID string) error

	// messages
	ChatAddMessage(ctx context.Context, msg types.IncomingChatMessage) error
	ChatGetMessages(ctx context.Context, chatID string, startFrom *time.Time, limit int) (types.ChatPage, error)

	// chat
	GetChat(ctx context.Context, chatID string) (types.Chat, error)
	CreateChat(ctx context.Context, chat types.Chat) (types.Chat, error)
	UpdateChat(ctx context.Context, chat types.Chat) error
	DeleteChat(ctx context.Context, chatID string) error

	AddToChatAttendees(ctx context.Context, userID string, chatID string) error
	RemoveFromChatAttendees(ctx context.Context, userID string, chatID string) error

	GetChatAttendees(ctx context.Context, chatID string) ([]types.Attendee, error)
	GetChatFullInfo(ctx context.Context, chatID string) (types.ChatFullInfo, error)

	GetUserChats(ctx context.Context, userID string) (types.UserChats, error)

	ApplyNotification(ctx context.Context, nType types.NotificationType, userID string, chatID string) ([]string, error)
}

// Factory

func NewDB(ctx context.Context, realtime *db.Client, projectID string, testMode bool, opts ...option.ClientOption) (DB, error) {
	fs, err := firestore.NewClient(ctx, projectID, opts...)
	if err != nil {
		return nil, err
	}

	// 1000 entities in the cache with 10s expiration
	cache := cache.New(1000, 10)

	realtimeRef := realtime.NewRef("/messages")

	if testMode {
		return &dbImpl{
			fs:          fs,
			users:       fs.Collection(usersTestCollection),
			cache:       cache,
			chats:       fs.Collection(chatsTestCollection),
			idGen:       utils.UUIDIDGen{},
			realtimeRef: realtimeRef,
		}, nil
	} else {
		return &dbImpl{
			fs:          fs,
			users:       fs.Collection(usersProdCollection),
			cache:       cache,
			chats:       fs.Collection(chatsProdCollection),
			idGen:       utils.UUIDIDGen{},
			realtimeRef: realtimeRef,
		}, nil
	}
}

// impl

type dbImpl struct {
	fs          *firestore.Client
	chats       *firestore.CollectionRef
	users       *firestore.CollectionRef
	cache       cache.Cache
	idGen       utils.IDGen
	realtimeRef *db.Ref
}

func (db *dbImpl) Close() error {
	if db != nil && db.fs != nil {
		return db.fs.Close()
	}
	return nil
}