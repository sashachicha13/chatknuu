package db

import (
	"context"
	"fmt"

	"google.golang.org/api/iterator"

	"cloud.google.com/go/firestore"
	"github.com/ChatKNU/pkg/types"
)

const (
	subEvents = "events"
)

func (db *dbImpl) ApplyNotification(ctx context.Context, nType types.NotificationType, userID string, chatID string) ([]string, error) {
	switch nType {
	case types.NotificationUserLeaveChat, types.NotificationUserJoinChat, types.NotificationUserCloseChat:
		attendees, err := db.applyAttendeesNotification(ctx, nType, userID, chatID)
		if err != nil {
			return nil, err
		}
		return attendees, db.addChatServiceMessage(ctx, nType, userID, chatID)
	case types.NotificationCreateChat:
		err := db.addChatServiceMessage(ctx, nType, userID, chatID)
		return nil, err
	default:
		return nil, types.ErrUnsupportedNotificationType
	}
}

func (db *dbImpl) GetNotifications(ctx context.Context, userID string) ([]types.Notification, error) {
	query := db.users.Doc(userID).Collection(subEvents).OrderBy("timestamp", firestore.Desc).Limit(100)
	iter := query.Documents(ctx)

	result := make([]types.Notification, 0, 100)

	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}

		var p types.Notification
		err = doc.DataTo(&p)
		if err != nil {
			return nil, err
		}
		p.ID = doc.Ref.ID

		result = append(result, p)
	}
	return result, nil
}

func (db *dbImpl) DeleteNotification(ctx context.Context, userID, ID string) error {
	docRef := db.users.Doc(userID).Collection("events").Doc(ID)
	_, err := docRef.Delete(ctx)
	return err
}

// impl

func (db *dbImpl) applyAttendeesNotification(ctx context.Context, nType types.NotificationType, userID string, chatID string) ([]string, error) {
	chat, err := db.GetChatFullInfo(ctx, chatID)
	if err != nil {
		return nil, err
	}

	affectedUsers := make([]string, 0, len(chat.Attendees))

	batch := db.fs.Batch()

	data := map[string]interface{}{
		"timestamp": firestore.ServerTimestamp,
		"type":      nType,
		"user_id":   userID,
		"chat_id":   chatID,
	}

	handledInstigator := false

	for _, attendee := range chat.Attendees {
		docRef := db.users.Doc(attendee.UserID).Collection(subEvents).NewDoc()
		batch.Set(docRef, data, firestore.MergeAll)
		affectedUsers = append(affectedUsers, attendee.UserID)
		if attendee.UserID == userID {
			handledInstigator = true
		}
	}

	docRef := db.users.Doc(chat.Chat.CreatorID).Collection(subEvents).NewDoc()
	batch.Set(docRef, data, firestore.MergeAll)
	if chat.Chat.CreatorID == userID {
		handledInstigator = true
	}
	affectedUsers = append(affectedUsers, chat.Chat.CreatorID)

	if !handledInstigator {
		docRef := db.users.Doc(userID).Collection(subEvents).NewDoc()
		batch.Set(docRef, data, firestore.MergeAll)
	}

	_, err = batch.Commit(ctx)
	return affectedUsers, err
}

var serviceChatMessages = map[types.NotificationType]string{
	types.NotificationCreateChat:    "Chat created",
	types.NotificationUserJoinChat:  "Good news! New dude is here! %s joined Chat",
	types.NotificationUserLeaveChat: "Uuups... %s left Chat",
}

func (db *dbImpl) addChatServiceMessage(ctx context.Context, nType types.NotificationType, userID string, chatID string) error {
	chatMessage := types.IncomingChatMessage{
		ChatID: chatID,
		IsBot:   true,
		LocalID: db.idGen.StringID(),
	}

	addMessage := false

	switch nType {
	case types.NotificationCreateChat:
		chatMessage.Text = serviceChatMessages[types.NotificationCreateChat]
		addMessage = true
	case types.NotificationUserJoinChat, types.NotificationUserLeaveChat:
		profile, err := db.GetProfile(ctx, userID)
		if err != nil {
			return err
		}
		chatMessage.Text = fmt.Sprintf(serviceChatMessages[nType], profile.Name)
		addMessage = true
	}

	if addMessage {
		return db.ChatAddMessage(ctx, chatMessage)
	}

	return nil
}
